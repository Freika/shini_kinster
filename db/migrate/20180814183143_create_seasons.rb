class CreateSeasons < ActiveRecord::Migration[5.2]
  def change
    create_table :seasons do |t|
      t.string :title
      t.integer :show_id
      t.datetime :started_at

      t.timestamps
    end
    add_index :seasons, :show_id
  end
end
