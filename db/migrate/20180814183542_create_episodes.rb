class CreateEpisodes < ActiveRecord::Migration[5.2]
  def change
    create_table :episodes do |t|
      t.string :title
      t.integer :season_id
      t.integer :episode_number

      t.timestamps
    end
    add_index :episodes, :season_id
    add_index :episodes, :episode_number
  end
end
