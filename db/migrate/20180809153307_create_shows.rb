class CreateShows < ActiveRecord::Migration[5.2]
  def change
    create_table :shows do |t|
      t.string :title
      t.datetime :started_at
      t.datetime :finished_at
      t.text :description
      t.integer :director_id
      t.integer :screenwriter_id
      t.integer :genre_id
      t.boolean :is_finished

      t.timestamps
    end
    add_index :shows, :director_id
    add_index :shows, :screenwriter_id
    add_index :shows, :genre_id
  end
end
