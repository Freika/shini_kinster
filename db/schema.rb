# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_08_14_183542) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "actors", force: :cascade do |t|
    t.string "full_name"
    t.datetime "born_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "show_id"
  end

  create_table "directors", force: :cascade do |t|
    t.string "full_name"
    t.datetime "born_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "episodes", force: :cascade do |t|
    t.string "title"
    t.integer "season_id"
    t.integer "episode_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["episode_number"], name: "index_episodes_on_episode_number"
    t.index ["season_id"], name: "index_episodes_on_season_id"
  end

  create_table "genres", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "screenwriters", force: :cascade do |t|
    t.string "full_name"
    t.datetime "born_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "seasons", force: :cascade do |t|
    t.string "title"
    t.integer "show_id"
    t.datetime "started_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["show_id"], name: "index_seasons_on_show_id"
  end

  create_table "shows", force: :cascade do |t|
    t.string "title"
    t.datetime "started_at"
    t.datetime "finished_at"
    t.text "description"
    t.integer "director_id"
    t.integer "screenwriter_id"
    t.integer "genre_id"
    t.boolean "is_finished"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["director_id"], name: "index_shows_on_director_id"
    t.index ["genre_id"], name: "index_shows_on_genre_id"
    t.index ["screenwriter_id"], name: "index_shows_on_screenwriter_id"
  end

end
