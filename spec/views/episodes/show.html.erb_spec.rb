require 'rails_helper'

RSpec.describe "episodes/show", type: :view do
  before(:each) do
    @episode = assign(:episode, Episode.create!(
      :title => "Title",
      :season_id => 2,
      :episode_number => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
  end
end
