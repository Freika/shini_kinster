require 'rails_helper'

RSpec.describe "episodes/edit", type: :view do
  before(:each) do
    @episode = assign(:episode, Episode.create!(
      :title => "MyString",
      :season_id => 1,
      :episode_number => 1
    ))
  end

  it "renders the edit episode form" do
    render

    assert_select "form[action=?][method=?]", episode_path(@episode), "post" do

      assert_select "input[name=?]", "episode[title]"

      assert_select "input[name=?]", "episode[season_id]"

      assert_select "input[name=?]", "episode[episode_number]"
    end
  end
end
