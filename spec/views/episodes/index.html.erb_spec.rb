require 'rails_helper'

RSpec.describe "episodes/index", type: :view do
  before(:each) do
    assign(:episodes, [
      Episode.create!(
        :title => "Title",
        :season_id => 2,
        :episode_number => 3
      ),
      Episode.create!(
        :title => "Title",
        :season_id => 2,
        :episode_number => 3
      )
    ])
  end

  it "renders a list of episodes" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
