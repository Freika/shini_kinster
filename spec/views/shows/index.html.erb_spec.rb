require 'rails_helper'

RSpec.describe "shows/index", type: :view do
  before(:each) do
    assign(:shows, [
      Show.create!(
        :title => "Title",
        :description => "MyText",
        :director_id => 2,
        :screenwriter_id => 3,
        :is_finished => false
      ),
      Show.create!(
        :title => "Title",
        :description => "MyText",
        :director_id => 2,
        :screenwriter_id => 3,
        :is_finished => false
      )
    ])
  end

  it "renders a list of shows" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
