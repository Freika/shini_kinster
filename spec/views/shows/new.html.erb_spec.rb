require 'rails_helper'

RSpec.describe "shows/new", type: :view do
  before(:each) do
    assign(:show, Show.new(
      :title => "MyString",
      :description => "MyText",
      :director_id => 1,
      :screenwriter_id => 1,
      :is_finished => false
    ))
  end

  it "renders new show form" do
    render

    assert_select "form[action=?][method=?]", shows_path, "post" do

      assert_select "input[name=?]", "show[title]"

      assert_select "textarea[name=?]", "show[description]"

      assert_select "input[name=?]", "show[director_id]"

      assert_select "input[name=?]", "show[screenwriter_id]"

      assert_select "input[name=?]", "show[is_finished]"
    end
  end
end
