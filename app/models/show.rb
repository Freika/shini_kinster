class Show < ApplicationRecord
  has_many :actors
  has_many :episodes
  has_many :seasons
  belongs_to :screenwriter
  belongs_to :director
  belongs_to :genre

  def show_finished_at
    finished_at.present? ? finished_at.year : 'N/A'
  end
end
