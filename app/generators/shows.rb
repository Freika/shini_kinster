class Shows
  def self.run
    drama_genre   = Genre.find_by(title: 'Драма')
    fantasy_genre = Genre.find_by(title: 'Фэнтези')

    bernstain = Director.find_by(full_name: 'Адам Бернштейн')
    krechmer = Director.find_by(full_name: 'Джон Т. Кречмер')
    tailor = Director.find_by(full_name: 'Алан Тейлор')

    gilligan = Screenwriter.find_by(full_name: 'Винс Гиллиган')
    benioff = Screenwriter.find_by(full_name: 'Дэвид Бениофф')
    berdge = Screenwriter.find_by(full_name: 'Констанс М. Бёрдж')

    Show.create(
      title: "Во все тяжкие",
      started_at: Date.new(2008),
      finished_at: Date.new(2014),
      description: "In the no-holds-barred world of Walt White",
      director_id: bernstain.id,
      screenwriter_id: gilligan.id,
      is_finished: true,
      genre_id: drama_genre.id
    )

    Show.create(
      title: "Зачарованные",
      started_at: Date.new(1998),
      finished_at: Date.new(2006),
      description: "Their mother always told them they were special..",
      director_id: krechmer.id,
      screenwriter_id: benioff.id,
      is_finished: true,
      genre_id: fantasy_genre.id
    )

    Show.create(
      title: "Игра престолов",
      started_at: Date.new(2011),
      finished_at: nil,
      description: "Зима близко",
      director_id: tailor.id,
      screenwriter_id: berdge.id,
      is_finished: true,
      genre_id: fantasy_genre.id
    )
  end
end
