class Directors
  def self.run
    Director.create(full_name: 'Адам Бернштейн', born_at: Date.new(1960))
    Director.create(full_name: 'Джон Т. Кречмер', born_at: Date.new(1954))
    Director.create(full_name: 'Алан Тейлор', born_at: Date.new(1965))
  end
end
