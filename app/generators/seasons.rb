class Seasons
  def self.run
    brba = Show.find_by(title: "Во все тяжкие")
    charmed = Show.find_by(title: "Зачарованные")
    game = Show.find_by(title: "Игра престолов")

    Season.create(title: "Во все тяжкие 1", show_id: brba.id, started_at: Date.new(2008))
    Season.create(title: "Во все тяжкие 2", show_id: brba.id, started_at: Date.new(2008))
    Season.create(title: "Зачарованные 1", show_id: charmed.id, started_at: Date.new(1998))
    Season.create(title: "Зачарованные 2", show_id: charmed.id, started_at: Date.new(1998))
    Season.create(title: "Зачарованные 3", show_id: charmed.id, started_at: Date.new(1998))
    Season.create(title: "Зачарованные 4", show_id: charmed.id, started_at: Date.new(1998))
    Season.create(title: "Игра престолов 1", show_id: game.id, started_at: Date.new(2011))
    Season.create(title: "Игра престолов 2", show_id: game.id, started_at: Date.new(2011))
    Season.create(title: "Игра престолов 3", show_id: game.id, started_at: Date.new(2011))
    Season.create(title: "Игра престолов 4", show_id: game.id, started_at: Date.new(2011))
  end
end
