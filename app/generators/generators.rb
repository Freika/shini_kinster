class Generators
  def self.run
    Generators::Genres.run unless Genre.any?
    Generators::Screenwriters.run unless Screenwriter.any?
    Generators::Directors.run unless Director.any?
    Generators::Shows.run unless Show.any?
    Generators::Seasons.run unless Season.any?
    Generators::Actors.run unless Actor.any?
    Generators::Episodes.run unless Episode.any?
  end
end
