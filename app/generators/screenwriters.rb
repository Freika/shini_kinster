class Screenwriters
  def self.run
    Screenwriter.create(full_name: 'Винс Гиллиган', born_at: Date.new(1967))
    Screenwriter.create(full_name: 'Дэвид Бениофф', born_at: Date.new(1970))
    Screenwriter.create(full_name: 'Констанс М. Бёрдж', born_at: Date.new(1957))
  end
end
