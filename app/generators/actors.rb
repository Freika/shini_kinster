class Actors
  def self.run
    brba    = Show.find_by(title: "Во все тяжкие")
    charmed = Show.find_by(title: "Зачарованные")
    game    = Show.find_by(title: "Игра престолов")

    Actor.create(full_name: "Лина Хиди", born_at: Date.new(1973), show_id: game.id)
    Actor.create(full_name: "Эмилия Кларк", born_at: Date.new(1986), show_id: game.id)
    Actor.create(full_name: "Питер Динклейдж", born_at: Date.new(1969), show_id: game.id)
    Actor.create(full_name: "Роуз МакГоун", born_at: Date.new(1973), show_id: charmed.id)
    Actor.create(full_name: "Холли Мари Комбс", born_at: Date.new(1973), show_id: charmed.id)
    Actor.create(full_name: "Алисса Милано", born_at: Date.new(1972), show_id: charmed.id)
    Actor.create(full_name: "Аарон Пол", born_at: Date.new(1979), show_id: brba.id)
    Actor.create(full_name: "Анна Ганн", born_at: Date.new(1968), show_id: brba.id)
    Actor.create(full_name: "Брайан Крэстон", born_at: Date.new(1956), show_id: brba.id)
  end
end
